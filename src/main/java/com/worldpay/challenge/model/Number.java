package com.worldpay.challenge.model;

/**
 * The enumeration encapsulates all the base numbers that can be converted directly into a string
 */
public enum Number {

	ZERO    ("",      0),
	ONE     ("one",   1),
	TWO     ("two",   2), 
	THREE   ("three", 3), 
	FOUR    ("four",  4),
	FIVE    ("five",  5),
	SIX     ("six",   6),
	SEVEN   ("seven", 7),
	EIGHT   ("eight", 8),
	NINE    ("nine",  9),
	
	TEN        ("ten",10),
	ELEVEN     ("eleven",11),
	TWELVE     ("twelve",12),
	THIRTEEN   ("thirteen",13),
	FOURTEEN   ("fourteen",14),
	FIFTEEN    ("fifteen",15),
	SIXTEEN    ("sixteen",16),
	SEVENTEEN  ("seventeen",17),
	EIGHTEEN   ("eighteen",18),
	NINETEEN   ("nineteen",19),
	
	TWENTY  ("twenty", 20),
	THIRTY  ("thirty", 30),
	FORTY   ("forty", 40),
	FIFTY   ("fifty", 50),
	SIXTY   ("sixty", 60),
	SEVENTY ("seventy", 70),
	EIGHTY  ("eighty", 80),
	NINTY   ("ninty", 90);
	
	
	private final String name;
	private final int number;
	
	private Number(String name, int number) {
		this.name = name;
		this.number = number;
	}
	
	/**
	 * @return the decimal given the number
	 */
	public static Number getNumber(int number) {
		for (Number num : values()) {
			if(num.getNumber() == number) {
				return num;
			}
		}
		return null;
	}

	/**
	 * @return the power
	 */
	public int getNumber() {
		return number;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
