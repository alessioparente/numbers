package com.worldpay.challenge.model;

/**
 * The enumeration encapsulates all the specific words needed to build a number string 
 * and that are not part of any pattern. 
 */
public enum Words {

	AND("and"),
	SPACE(" "),
	HUNDRED(" hundred"),
	ZERO("zero");
	
	private final String name;
	
	private Words(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
