package com.worldpay.challenge.model;

/**
 * The enumeration encapsulates all the names for the thousand group such as thousand or million
 */
public enum Thousands {
	
	ZERO    ("",0), 
	THREE   ("thousand",3), 
	SIX     ("million",6);

	
	
	private final String name;
	private final int power;
	
	private Thousands(String name, int power) {
		this.name = name;
		this.power = power;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the thousand given the power
	 */
	public static Thousands getName(int power) {
		for (Thousands thousand : values()) {
			if(thousand.getPower() == power) {
				return thousand;
			}
		}
		return null;
	}

	/**
	 * @return the power
	 */
	public int getPower() {
		return power;
	}

}
