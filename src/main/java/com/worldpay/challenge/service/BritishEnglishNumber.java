package com.worldpay.challenge.service;


import com.worldpay.challenge.logic.ConvertNumber;
import com.worldpay.challenge.validate.OutOfRangeException;
import com.worldpay.challenge.validate.ParseException;
import com.worldpay.challenge.validate.ValidateNumber;



/**
 * The class implements the solution of the Worldpay Techincal Challenge.
 * 
 * @author alessio
 *
 */
public class BritishEnglishNumber {
	
	ValidateNumber validateNumber;

	ConvertNumber convertNumber;

	
	/**
	 * Constructor
	 */
	public BritishEnglishNumber() {
		validateNumber = new ValidateNumber();
		convertNumber = new ConvertNumber();
	}
	
	/**
	 * 
	 * @param input the string number to convert into words
	 * @return the words representing the input number
	 * @throws ParseException when is not possible to parse the input String 
	 */
	public String numberToBritishEnglishWords(String input) throws ParseException, OutOfRangeException {

		long number = validateNumber.validate(input);
		String result = convertNumber.convert(number);
		return result;
	}
}
