package com.worldpay.challenge.validate;

/**
 * The Parse Exception class 
 * 
 * @author alessio
 *
 */
public class ParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2562094512213074161L;

	/**
	 * Constructor
	 * @param message a message
	 */
	public ParseException(String message) {
		super(message);
	}

}
