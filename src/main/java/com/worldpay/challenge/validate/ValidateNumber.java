package com.worldpay.challenge.validate;

/**
 * 
 * The class is responsible to validate the number
 * 
 * @author alessio
 *
 */
public class ValidateNumber {

	/**
	 * The class validates a String and returns the long value 
	 * @param input the number to validate
	 * @return the validated number as a long
	 * @throws ParseException if is not possible to validate the number
	 * @throws OutOfRangeException if the number exceeds 1000000000
	 */
	public long validate(String input) throws ParseException, OutOfRangeException {
		
		try {
			long number = Long.parseLong(input);
			if(number > 999999999 || number < 0) {
				throw new OutOfRangeException(String.valueOf(number));
			}
			return number;
		} catch(NumberFormatException e) {
			throw new ParseException(e.getMessage());
		}
	}
}
