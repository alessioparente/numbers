package com.worldpay.challenge.validate;

/**
 * The Out Of Range Exception class 
 * 
 * @author alessio
 *
 */
public class OutOfRangeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6389718718605556070L;

	/**
	 * Constructor
	 * @param message a message
	 */
	public OutOfRangeException(String message) {
		super(message);
	}

}
