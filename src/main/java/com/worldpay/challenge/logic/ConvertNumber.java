package com.worldpay.challenge.logic;

import com.worldpay.challenge.model.Words;
import com.worldpay.challenge.model.Thousands;
import com.worldpay.challenge.model.Number;

/**
 * The class contains all the logic to convert a number into its British representation
 * 
 * @author alessio
 *
 */
public class ConvertNumber {


	/**
	 * Converts the number into a string
	 * @param number the input number can be positive or zero.
	 * @return the converted number
	 */
	public String convert(long number) {
		if(number == 0) {
			return Words.ZERO.getName();
		} else {
			return convert(number, 0);
		}
	}

	/**
	 * Converts a number into a String
	 * @param number the input number to convert given its power 10
	 * @param thousands a multiple of three indicating which thousand group to convert 
	 * @return
	 */
	String convert(long number, int thousands) {
		
		if(number != 0) {
			long leftValue = number / 1000;
			int rightValue = (int) (number - (leftValue * 1000));
			String left = convert(leftValue, thousands + 3);
			String right = convertThousand(rightValue, thousands, left.length() > 0);
			return concatSpace(left, right);
		} else {
			return "";
		}
	}

	/**
	 * Converts a number into a String
	 * @param value the input number to convert, from 0 to 999
	 * @param thousands the thousand group to use when converting the number
	 * @return the string representing the number 
	 */
	String convertThousand(int value, int thousands, boolean previous) {
		
		if(value == 0) {
			return "";
		}
		
		int hundred = ((int)value / 100);
		String hundredName = getHundredName(hundred);

		int decimal = value - hundred * 100; 
		String decimalName = getDecimalName(decimal);
		
		String thousandsName = Thousands.getName(thousands).getName();

		return concatSpace(concatAnd(hundredName, decimalName, previous), thousandsName);
	}

	/**
	 * Converts a hundred number into its string representation
	 * @param hundred a number between 0 and 9
	 * @return the string representation of the hundreds
	 */
	String getHundredName(int hundred) {
		if(hundred > 0) {
			return getNumberName(hundred) + Words.HUNDRED.getName();
		} else {
			return "";
		}
	}
	
	/**
	 * Converts a two digits number into its string representation
	 * @param value a number between 0 and 99
	 * @return the string representation of the decimal number
	 */
	String getDecimalName(int value) {
		if(value >= 20) {
			int decimal = (value / 10) * 10;
			int unit = value - decimal;
			return concatSpace(getNumberName(decimal), getNumberName(unit));
		} else {
			return getNumberName(value);
		}
	}

	/**
	 * Converts the decimal part of a two digits number into its string representation
	 * @param decimal a value between 20 and 90, multiple of 10
	 * @return the decimal name to represent the number
	 */
	String getNumberName(int decimal) {
		return Number.getNumber(decimal).getName();
	}
	
	/**
	 * Concats two strings with a space if needed
	 * @param left left part to concat
	 * @param right right part to concat
	 * @return the concat string
	 */
	String concatSpace(String left, String right) {
		return concat(left, Words.SPACE.getName(), right);
	}

	/**
	 * Concats two strings with the word AND if needed
	 * @param left left part to concat
	 * @param right right part to concat
	 * @return the concat string
	 */
	String concatAnd(String left, String right, boolean previous) {
		
		if(previous && left.length() == 0 && right.length() > 0) {
			return Words.AND.getName() + Words.SPACE.getName() + right;
		} else {
			return concat(left, Words.SPACE.getName() + Words.AND.getName() + Words.SPACE.getName(), right);
		}
	}

	/**
	 * Concats two strings with a middle string in between if both are not empty
	 * @param left left part to concat
	 * @param middle middle part to concat
	 * @param right right part to concat
	 * @return the concat string
	 */
	String concat(String left, String middle, String right) {
		if(!left.equals("") && !right.equals("")) {
			return left + middle + right;	
		} else if(left.equals("")) {
			return right;
		} else {
			return left;	
		}
	}

}
