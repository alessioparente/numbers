package com.worldpay.challenge.validate;

import junit.framework.Assert;

import org.junit.Test;


public class ValidateNumberTest {

	ValidateNumber validateNumber = new ValidateNumber();

	@Test(expected = ParseException.class)
	public void testValidateEx1() throws ParseException, OutOfRangeException {
		validateNumber.validate("abc");
	}	
	@Test(expected = OutOfRangeException.class)
	public void testValidateMaxPositiveException() throws ParseException, OutOfRangeException {
		validateNumber.validate("1000000000");
	}	
	@Test(expected = OutOfRangeException.class)
	public void testValidateMinNegativeException() throws ParseException, OutOfRangeException {
		validateNumber.validate("-1");
	}	
	@Test
	public void testValidateZero() throws ParseException, OutOfRangeException {
		long actual = validateNumber.validate("0");
		Assert.assertEquals(0, actual);
	}	
	@Test
	public void testValidatePositive() throws ParseException, OutOfRangeException {
		long actual = validateNumber.validate("1");
		Assert.assertEquals(1, actual);
	}	
	@Test
	public void testValidateMaxPositive() throws ParseException, OutOfRangeException {
		long actual = validateNumber.validate("999999999");
		Assert.assertEquals(999999999, actual);
	}	
}
