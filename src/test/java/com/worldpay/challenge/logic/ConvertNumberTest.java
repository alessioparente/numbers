package com.worldpay.challenge.logic;

import junit.framework.Assert;

import org.junit.Test;



public class ConvertNumberTest {

	ConvertNumber convertNumber = new ConvertNumber();

	@Test
	public void testConvertZero() {
		Assert.assertEquals("zero",convertNumber.convert(0));
	}
	@Test
	public void testConvertOne() {
		Assert.assertEquals("one",convertNumber.convert(1));
	}
	@Test
	public void testConvert21() {
		Assert.assertEquals("twenty one",convertNumber.convert(21));
	}
	@Test
	public void testConvert105() {
		Assert.assertEquals("one hundred and five",convertNumber.convert(105));
	}
	@Test
	public void testConvert123() {
		Assert.assertEquals("one hundred and twenty three",convertNumber.convert(123));
	}
	@Test
	public void testConver1005() {
		Assert.assertEquals("one thousand and five",convertNumber.convert(1005));
	}
	@Test
	public void testConvert1042() {
		Assert.assertEquals("one thousand and forty two",convertNumber.convert(1042));
	}
	@Test
	public void testConvert1105() {
		Assert.assertEquals("one thousand one hundred and five",convertNumber.convert(1105));
	}
	@Test
	public void testConvert56945781() {
		Assert.assertEquals("fifty six million nine hundred and forty five thousand seven hundred and eighty one",convertNumber.convert(56945781));
	}
	@Test
	public void testConvert999999999() {
		Assert.assertEquals("nine hundred and ninty nine million nine hundred and ninty nine thousand nine hundred and ninty nine",convertNumber.convert(999999999));
	}
	@Test
	public void testConvertThousand0() {
		Assert.assertEquals("",convertNumber.convertThousand(0,0, false));
	}
	@Test
	public void testConvertThousand200() {
		Assert.assertEquals("two hundred",convertNumber.convertThousand(200,0, false));
	}
	@Test
	public void testConvertThousand23() {
		Assert.assertEquals("twenty three",convertNumber.convertThousand(23,0, false));
	}
	@Test
	public void testConvertThousand123() {
		Assert.assertEquals("one hundred and twenty three",convertNumber.convertThousand(123,0, false));
	}
	@Test
	public void testConvertThousand200000() {
		Assert.assertEquals("two hundred thousand",convertNumber.convertThousand(200,3, false));
	}

	@Test
	public void testConvertThousand23000000() {
		Assert.assertEquals("twenty three million",convertNumber.convertThousand(23,6, false));
	}
	@Test
	public void testConvertThousand123000() {
		Assert.assertEquals("one hundred and twenty three thousand",convertNumber.convertThousand(123,3, false));
	}
	@Test
	public void testConvertThousand123000000() {
		Assert.assertEquals("one hundred and twenty three million",convertNumber.convertThousand(123,6, false));
	}

	@Test
	public void testGetHundredName0() {
		Assert.assertEquals("",convertNumber.getHundredName(0));
	}

	@Test
	public void testGetHundredName1() {
		Assert.assertEquals("one hundred",convertNumber.getHundredName(1));
	}

	@Test
	public void testGetHundredName2() {
		Assert.assertEquals("two hundred",convertNumber.getHundredName(2));
	}

	@Test
	public void testGetDecimalName19() {
		Assert.assertEquals("nineteen",convertNumber.getDecimalName(19));
	}

	@Test
	public void testGetDecimalName20() {
		Assert.assertEquals("twenty",convertNumber.getDecimalName(20));
	}

	@Test
	public void testGetDecimalName21() {
		Assert.assertEquals("twenty one",convertNumber.getDecimalName(21));
	}

	@Test
	public void testGetNumberName0() {
		Assert.assertEquals("",convertNumber.getNumberName(0));
	}

	@Test
	public void testGetNumberName1() {
		Assert.assertEquals("one",convertNumber.getNumberName(1));
	}

	@Test
	public void testGetNumberName2() {
		Assert.assertEquals("two",convertNumber.getNumberName(2));
	}

	@Test
	public void testGetNumberName3() {
		Assert.assertEquals("three",convertNumber.getNumberName(3));
	}

	@Test
	public void testGetNumberName4() {
		Assert.assertEquals("four",convertNumber.getNumberName(4));
	}

	@Test
	public void testGetNumberName5() {
		Assert.assertEquals("five",convertNumber.getNumberName(5));
	}

	@Test
	public void testGetNumberName6() {
		Assert.assertEquals("six",convertNumber.getNumberName(6));
	}

	@Test
	public void testGetNumberName7() {
		Assert.assertEquals("seven",convertNumber.getNumberName(7));
	}

	@Test
	public void testGetNumberName8() {
		Assert.assertEquals("eight",convertNumber.getNumberName(8));
	}

	@Test
	public void testGetNumberName9() {
		Assert.assertEquals("nine",convertNumber.getNumberName(9));
	}

	@Test
	public void testGetNumberName10() {
		Assert.assertEquals("ten",convertNumber.getNumberName(10));
	}

	@Test
	public void testGetNumberName11() {
		Assert.assertEquals("eleven",convertNumber.getNumberName(11));
	}

	@Test
	public void testGetNumberName12() {
		Assert.assertEquals("twelve",convertNumber.getNumberName(12));
	}

	@Test
	public void testGetNumberName13() {
		Assert.assertEquals("thirteen",convertNumber.getNumberName(13));
	}

	@Test
	public void testGetNumberName14() {
		Assert.assertEquals("fourteen",convertNumber.getNumberName(14));
	}

	@Test
	public void testGetNumberName15() {
		Assert.assertEquals("fifteen",convertNumber.getNumberName(15));
	}

	@Test
	public void testGetNumberName16() {
		Assert.assertEquals("sixteen",convertNumber.getNumberName(16));
	}
	
	@Test
	public void testGetNumberName17() {
		Assert.assertEquals("seventeen",convertNumber.getNumberName(17));
	}

	@Test
	public void testGetNumberName18() {
		Assert.assertEquals("eighteen",convertNumber.getNumberName(18));
	}

	@Test
	public void testGetNumberName19() {
		Assert.assertEquals("nineteen",convertNumber.getNumberName(19));
	}

	@Test
	public void testGetNumberName20() {
		Assert.assertEquals("twenty",convertNumber.getNumberName(20));
	}

	@Test
	public void testGetNumberName30() {
		Assert.assertEquals("thirty",convertNumber.getNumberName(30));
	}

	@Test
	public void testGetNumberName40() {
		Assert.assertEquals("forty",convertNumber.getNumberName(40));
	}

	@Test
	public void testGetNumberName50() {
		Assert.assertEquals("fifty",convertNumber.getNumberName(50));
	}

	@Test
	public void testGetNumberName60() {
		Assert.assertEquals("sixty",convertNumber.getNumberName(60));
	}

	@Test
	public void testGetNumberName70() {
		Assert.assertEquals("seventy",convertNumber.getNumberName(70));
	}

	@Test
	public void testGetNumberName80() {
		Assert.assertEquals("eighty",convertNumber.getNumberName(80));
	}

	@Test
	public void testGetNumberName90() {
		Assert.assertEquals("ninty",convertNumber.getNumberName(90));
	}

	@Test
	public void testConcatAndFalse() {
		Assert.assertEquals("a and c",convertNumber.concatAnd("a","c", false));
	}

	@Test
	public void testConcatAndTrue() {
		Assert.assertEquals("a and c",convertNumber.concatAnd("a","c", true));
	}
	
	@Test
	public void testConcatAndAFalse() {
		Assert.assertEquals("a",convertNumber.concatAnd("a","", false));
	}

	@Test
	public void testConcatAndATrue() {
		Assert.assertEquals("a",convertNumber.concatAnd("a","", true));
	}

	@Test
	public void testConcatAndCFalse() {
		Assert.assertEquals("c",convertNumber.concatAnd("","c", false));
	}

	@Test
	public void testConcatAndCTrue() {
		Assert.assertEquals("and c",convertNumber.concatAnd("","c", true));
	}


	@Test
	public void testConcatSpace() {
		Assert.assertEquals("a c",convertNumber.concatSpace("a","c"));
	}

	@Test
	public void testConcat1() {
		Assert.assertEquals("abc",convertNumber.concat("a","b","c"));
	}

	@Test
	public void testConcat2() {
		Assert.assertEquals("a",convertNumber.concat("a","b",""));
	}

	@Test
	public void testConcat3() {
		Assert.assertEquals("ac",convertNumber.concat("a","","c"));
	}

	@Test
	public void testConcat4() {
		Assert.assertEquals("c",convertNumber.concat("","b","c"));
	}

	@Test
	public void testConcat5() {
		Assert.assertEquals("c",convertNumber.concat("","","c"));
	}

	@Test
	public void testConcat6() {
		Assert.assertEquals("",convertNumber.concat("","b",""));
	}

	@Test
	public void testConcat7() {
		Assert.assertEquals("a",convertNumber.concat("a","",""));
	}

	@Test
	public void testConcat8() {
		Assert.assertEquals("",convertNumber.concat("","",""));
	}


}
