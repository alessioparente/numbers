package com.worldpay.challenge.service;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.worldpay.challenge.logic.ConvertNumber;
import com.worldpay.challenge.validate.ParseException;
import com.worldpay.challenge.validate.ValidateNumber;



@RunWith(MockitoJUnitRunner.class)
public class BritishEnglishNumberTest {
	
	@Mock
	ValidateNumber validateNumber;

	@Mock
	ConvertNumber convertNumber;

	@InjectMocks
	BritishEnglishNumber britishEnglishNumber = new BritishEnglishNumber();
	
	@Test
	public void numberToBritishEnglishWordsTest0() throws Exception {
		Mockito.when(validateNumber.validate("12")).thenReturn(12l);
		Mockito.when(convertNumber.convert(12)).thenReturn("twelve");
		Assert.assertEquals("twelve", britishEnglishNumber.numberToBritishEnglishWords("12"));
	}
	
	@Test(expected = ParseException.class)
	public void numberToBritishEnglishWordsTest1() throws Exception {
		Mockito.when(validateNumber.validate("abc")).thenThrow(new ParseException(""));
		britishEnglishNumber.numberToBritishEnglishWords("abc");
	}
}
